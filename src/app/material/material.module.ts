import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout'
import { MatSidenavModule, 
         MatButtonModule,
         MatDividerModule,
         MatToolbarModule,
         MatIconModule, 
         MatMenuModule,
         MatListModule,
         MatCardModule
         } from '@angular/material';


const material = [
        MatSidenavModule,
        MatButtonModule,
        MatDividerModule,
        MatToolbarModule,
        MatIconModule,
        FlexLayoutModule,
        MatMenuModule,
        MatListModule,
        MatCardModule
        
]

@NgModule({
  imports: [ material ],
  exports: [ material ]
})
export class MaterialModule { }
