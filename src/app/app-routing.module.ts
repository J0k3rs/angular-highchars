import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DefaultComponent } from './layouts/default/default.component';
import { DashbordComponent } from './modules/dashbord/dashbord.component';
import { PostsComponent } from './modules/posts/posts.component';
import { AreaComponent } from './shared/widgets/area/area.component';


const routes: Routes = [{
  path: '',
  component: DefaultComponent,
  children: [{
    path: '',
    component: DashbordComponent
  }, {
    path: 'posts',
    component: PostsComponent
  }, {
    path: 'charts',
    component: AreaComponent
  }]
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
