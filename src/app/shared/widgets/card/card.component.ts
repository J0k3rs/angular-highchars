import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';

import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, Label } from 'ng2-charts';

declare var require: any;

@Component({
  selector: 'app-widget-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    /* Highcharts.chart('containers', this.containers); */
    setTimeout(() => {
      window.dispatchEvent(new Event('resize'));
    }, 300);
  }

  lineChartData: ChartDataSets[] = [
    { data: [85, 72, 78, 75, 77, 75], label: 'Crude oil prices' },
  ];

  lineChartLabels: Label[] = ['January', 'February', 'March', 'April', 'May', 'June'];

  lineChartOptions = {
    responsive: true,
  };

  lineChartColors: Color[] = [
    {
      borderColor: 'black',
      backgroundColor: 'rgba(255,255,0,0.28)',
    },
  ];

  lineChartLegend = true;
  lineChartPlugins = [];
  lineChartType = 'line';


  /*  public containers: any = {
     chart: {
       type: 'area'
     },
     title: {
       text: null
     },
     subtitle: {
       text: null
     },
     tooltip: {
       split: true,
       valueSuffix: ' millions'
     },
     exporting: {
       enabled: false
     },
     xAxis: {
       labels: {
         enabled: false
       },
       title: {
         text: null
       },
       startOnTick: false,
       endOnTick: false,
       tickOptions: []
     },
     yAxis: {
       labels: {
         enabled: false
       },
       title: {
         text: null
       },
       startOnTick: false,
       endOnTick: false,
       tickOptions: []
     },
     series: [{
       data: [43, 70, 50, 69]
     }]
   };
  */

}
