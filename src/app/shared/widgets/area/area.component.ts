import { Component, OnInit} from '@angular/core';
import { Chart } from 'angular-highcharts';
import * as Highcharts from 'highcharts';

declare var require: any;

@Component({
    selector: 'app-widget-area',
    templateUrl: './area.component.html',
    styleUrls: ['./area.component.css']
})
export class AreaComponent implements OnInit {

    constructor() { }

    ngOnInit() {
        /*     Highcharts.chart('container', this.options);
            Highcharts.chart('graphe1', this.chart1); */
        Highcharts.chart('graphe2', this.chart2);
        
        setTimeout(() => { 
            window.dispatchEvent(
                new Event('resize')
                ); 
            }, 300);
    }

    public chart2: any = {
        chart: {
            type: 'area'
        },
        title: {
            text: 'Random Data -  Historic and Estimated Worldwide Population Growth by Region'
        },
        subtitle: {
            text: 'Source: Demo'
        },
        /* xAxis: {
            categories: ['1750', '1800', '1850', '1900', '1950', '1999', '2050'],
            tickmarkPlacement: 'on',
            title: {
                enabled: false
            }
        },
        yAxis: {
            title: {
                text: 'Billions'
            },
            labels: {
                formatter: function () {
                    return this.value / 1000;
                }
            }
        }, */
        tooltip: {
            split: true,
            valueSuffix: ' millions'
        },
        /* plotOptions: {
            area: {
                stacking: 'normal',
                lineColor: '#666666',
                lineWidth: 1,
                marker: {
                    lineWidth: 1,
                    lineColor: '#666666'
                }
            }
        }, */
        series: [{
            name: 'Asia',
            data: [502, 635, 809, 947, 1402, 3634, 5268]
        }, {
            name: 'Africa',
            data: [106, 107, 111, 133, 221, 767, 1766]
        }, {
            name: 'Europe',
            data: [163, 203, 276, 408, 547, 729, 628]
        }, {
            name: 'America',
            data: [18, 31, 54, 156, 339, 818, 1201]
        }, {
            name: 'Oceania',
            data: [2, 2, 2, 6, 13, 30, 46]
        }]
    };
 
}













/* public chart1:any = {
      chart: {
        type: 'area'
      },
      title: {
        text: 'Linechart'
      },
      credits: {
        enabled: false
      },
      series: [
        {
          name: 'Area 1',
          type: "area",
          data: [1, 2, 3]
        }
      ]
    }; */























 /* public options: any = {
        Chart: {
          type: 'area',
          height: 700
        },
        title: {
          text: 'Evolution de la population'
        },
        credits: {
          enabled: false
        },
        xAxis: {
          categories: ['1750', '1800', '1850', '1900', '1950', '1999', '2050'],
          tickmarkPlacement: 'on',
          title: {
              enabled: false
          }
      },
        series: [{
          name: 'Asia',
          data: [502, 635, 809, 947, 1402, 3634, 5268]
      }, {
          name: 'Europe',
          data: [163, 203, 276, 408, 547, 729, 628]
      }, {
          name: 'America',
          data: [18, 31, 54, 156, 339, 818, 1201]
      }]
    
    } */