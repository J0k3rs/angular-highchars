import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { DefaultComponent } from 'src/app/layouts/default/default.component';
import { DashbordComponent } from 'src/app/modules/dashbord/dashbord.component';
import { RouterModule } from '@angular/router';
import { PostsComponent } from 'src/app/modules/posts/posts.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { MaterialModule } from 'src/app/material/material.module';


@NgModule({
  declarations: [
    DefaultComponent,
    DashbordComponent,
    PostsComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    MaterialModule
  ]
})
export class DefaultModule { }
